float ROTX = PI;                        
float ROTY = HALF_PI;
float SCALE = 0.00002;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////control the rotation and scale display of the model using the mouse
void mouseDragged() {
   
   if ((mouseY>screenGUI/2)&&(mouseY<screenH+(screenGUI/2))){
     if (mouseButton == LEFT) {
       ROTX += (mouseX - pmouseX) * 0.01;
       ROTY -= (mouseY - pmouseY) * 0.01;
     }
     if (mouseButton == RIGHT) {
       if (mouseY>pmouseY){
          SCALE = SCALE+0.000001;
       } else {
          SCALE = SCALE-0.000001;
       }
       if (SCALE>0.0001){ SCALE=0.0001;}
       if (SCALE<=0.0000015){ SCALE=0.0000015;}
     }
     redraw();
   }
}





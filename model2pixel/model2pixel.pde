String fileName = "mesh0ASCII";
////////////////////////////////////////////////////////////////////////////////
///load stl file and generate 3D.png. displays model and allows teeting of image
////////////////////////////////////////////////////////////////////////////////

/////////////////////////GUI
int screenH = 600;
int screenW = 800;
int screenGUI = 0;
/////////////////////////PIX
int colRangeMax = 16777215;
float modRangeMax = 16777.215;
int precisionVal = 1;
int graphHeight =512;
int displayBox = 16777216;
/////////////////////////MESH OBJECTS
ArrayList<meshObj> meshes;
meshObj mesh;
int loadMeshes = 1;

void setup() {

 noLoop();
 size(screenW, screenH,OPENGL);

 meshes = new ArrayList<meshObj>();
 initializeMeshes();

}

void draw() {
  
  background(0);
      
  pushMatrix();
    translate((width/6)*3,height/2,0);
    rotateY(ROTX);
    rotateX(ROTY);
    scale(SCALE);
    directionalLight(125, 125, 125, 0, 1, -1);
    ambientLight(75,75,75);
    
    noFill();
    stroke(255);
    box(displayBox);
   
     for (int i=0; i<meshes.size(); i++){   
       meshObj tempMesh = meshes.get(i);
       tempMesh.meshDraw();
     }
     
     noLights();
   popMatrix();
   
   for (int i=0; i<meshes.size(); i++){   
     meshObj tempMesh = meshes.get(i);
     image(tempMesh.pgGraph, 0, 0);
   }
   
}

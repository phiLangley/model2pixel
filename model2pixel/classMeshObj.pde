class meshObj{
  
  int index;
/////////////////////////IMAGE 
  PGraphics pgSquare, pgGraph;
  int pixColCount;
  int maxWidth=0;
  int maxWidthTemp=0;
/////////////////////////STL
  String txtSTL = fileName; 
/////////////////////////LISTS FOR SORTING FACES AND VERICES  
  ArrayList<String> fList;
  ArrayList<String> vList;
/////////////////////////LISTS FOR SORTING FACES AND VERICES  
  Float[][] stlData;
  String[] pixRowArray = new String[graphHeight];
  String[] pixRowArrayMap = new String[graphHeight];
/////////////////////////GEOMETRY 
  Float vertices[][];
  Float[] boundX = new Float[3];
  Float[] boundY = new Float[3];
  Float[] boundZ = new Float[3];
  Float[] boundMax = new Float[2];

  meshObj(int identity){
    
     fList = new ArrayList();
     vList = new ArrayList();
     
     index = identity;
     
     loadStlData();
     sortGeom();
     
     makePixelgraph();
     makePixelSquare();

  }
  
  void loadStlData(){
       
    /////////////////////////LOAD STL FILE  
    String lines[] = loadStrings("data/" + txtSTL + ".stl");
    /////////////////////////LOAD STL FILE 
    
    /////////////////////////SET MAX AND MIN VALUES FOR BOUNDING BOX CALCULATION
    boundX[0] = 16777216.0;
    boundX[1] = -16777216.0;
    boundY[0] = 16777216.0;
    boundY[1] = -16777216.0;
    boundZ[0] = 16777216.0;
    boundZ[1] = -16777216.0;
    
    int lineCount=0;
    String tempVert0[];
    String tempVert1[];
    String tempVert2[];
    
    while (lineCount<lines.length-3){
      
      lines[lineCount] = trim(lines[lineCount]);
      tempVert0 = split(lines[lineCount], " ");  
      
      if (tempVert0[0].equals("vertex")) {      
        
        tempVert1 = split(lines[lineCount+1], " ");
        tempVert2 = split(lines[lineCount+2], " ");
         
        vList.add(tempVert0[1] + "," + tempVert0[2] + "," + tempVert0[3]);  
        vList.add(tempVert1[1] + "," + tempVert1[2] + "," + tempVert1[3]);  
        vList.add(tempVert2[1] + "," + tempVert2[2] + "," + tempVert2[3]);     
        fList.add(
          tempVert0[1] + "," + tempVert0[2] + "," + tempVert0[3] + "," + 
          tempVert1[1] + "," + tempVert1[2] + "," + tempVert1[3] + "," + 
          tempVert2[1] + "," + tempVert2[2] + "," + tempVert2[3]
          );   
          
        lineCount+=3;
        
        /////////////////////////CALCULATE MIN AND MAX VALUES FOR X,Y,Z OF BOUNDING BOX
        if (boundX[0]>float(tempVert0[1])){boundX[0] = float(tempVert0[1]);}
        if (boundX[1]<float(tempVert0[1])){boundX[1] = float(tempVert0[1]);}
        if (boundX[0]>float(tempVert1[1])){boundX[0] = float(tempVert1[1]);}
        if (boundX[1]<float(tempVert1[1])){boundX[1] = float(tempVert1[1]);}
        if (boundX[0]>float(tempVert2[1])){boundX[0] = float(tempVert2[1]);}
        if (boundX[1]<float(tempVert2[1])){boundX[1] = float(tempVert2[1]);}       
        
        if (boundY[0]>float(tempVert0[2])){boundY[0] = float(tempVert0[2]);}
        if (boundY[1]<float(tempVert0[2])){boundY[1] = float(tempVert0[2]);}
        if (boundY[0]>float(tempVert1[2])){boundY[0] = float(tempVert1[2]);}
        if (boundY[1]<float(tempVert1[2])){boundY[1] = float(tempVert1[2]);}
        if (boundY[0]>float(tempVert2[2])){boundY[0] = float(tempVert2[2]);}
        if (boundY[1]<float(tempVert2[2])){boundY[1] = float(tempVert2[2]);} 
        
        if (boundZ[0]>float(tempVert0[3])){boundZ[0] = float(tempVert0[3]);}
        if (boundZ[1]<float(tempVert0[3])){boundZ[1] = float(tempVert0[3]);}
        if (boundZ[0]>float(tempVert1[3])){boundZ[0] = float(tempVert1[3]);}
        if (boundZ[1]<float(tempVert1[3])){boundZ[1] = float(tempVert1[3]);}
        if (boundZ[0]>float(tempVert2[3])){boundZ[0] = float(tempVert2[3]);}
        if (boundZ[1]<float(tempVert2[3])){boundZ[1] = float(tempVert2[3]);} 
          
      } else {
        lineCount++;
      } 

    }
   
//    println(fList.size());
//    println(vList.size());
    
    /////////////////////////CALCULATE LARGEST SIDE OF BOUNDING BOX
    boundX[2] = boundX[1] - boundX[0];
    boundY[2] = boundY[1] - boundY[0];
    boundZ[2] = boundZ[1] - boundZ[0];
    
    if (boundX[2]>boundY[2]){
      if (boundX[2]>boundZ[2]){
        boundMax[0] = boundX[0];
        boundMax[1] = boundX[1];
      } else {
        boundMax[0] = boundZ[0];
        boundMax[1] = boundZ[1];
      }
    } else {
      if (boundY[2]>boundZ[2]){
        boundMax[0] = boundY[0];
        boundMax[1] = boundY[1];
      } else {
        boundMax[0] = boundZ[0];
        boundMax[1] = boundZ[1];
      }
    }
 
//    println("X... " + boundX[0] + " ... " + boundX[1]);
//    println("X RANGE... " + boundX[2]);
//    println("Y... " + boundY[0] + " ... " + boundY[1]);
//    println("Y RANGE... " + boundY[2]);
//    println("Z... " + boundZ[0] + " ... " + boundZ[1]);
//    println("Z RANGE... " + boundZ[2]);    
//    println("MAX... " + boundMax[0] + " - " + boundMax[1]);   
   
  } 
  
  
  void makePixelSquare(){
    
    int dimX = int(sqrt(fList.size()*9)/9)*9;
    int dimY = int((fList.size()*9)/dimX)+1;    
 
//    println(dimX, dimY);
    
    int faceCount = 0;
    int pixCount = 0;
    
    float tempX, tempY, tempZ;
    color pixCol;
    int tempColX, tempColY, tempColZ;
    
    pgSquare = createGraphics(dimX, dimY);
    pgSquare.background(0,0);
    pgSquare.beginDraw();
  
    for (int i=0; i< dimY; i++) {
      for (int j=0; j< dimX; j+=9) { 
        
        if (faceCount< fList.size()){
          
          for (int k=0; k<3; k++) {
            
            String temp[] = split(fList.get(faceCount), ",");
            
            tempColX = int(map(float(temp[0+(k*3)]), -modRangeMax/2,  modRangeMax/2, -colRangeMax, -1));  
            pixCol = tempColX;
            pgSquare.set(j+0+(k*3), i, pixCol);     
            
            tempColY = int(map(float(temp[1+(k*3)]), -modRangeMax/2,  modRangeMax/2, -colRangeMax, -1)); 
            pixCol = tempColY;
            pgSquare.set(j+1+(k*3), i, pixCol);     
            
            tempColZ = int(map(float(temp[2+(k*3)]),  -modRangeMax/2,  modRangeMax/2, -colRangeMax, -1));   
            pixCol = tempColZ;
            pgSquare.set(j+2+(k*3), i, pixCol);     
  
          }
          
          faceCount++;
        }
         
      }
    }
 
    pgSquare.updatePixels();
    pgSquare.endDraw();
    pgSquare.save("data/loaded/" + txtSTL + "-PixelSquare.png");
    
  }

  void makePixelgraph(){
    
    ///CALCULATE MAXIMUM WIDTH OF GRAPH
    maxWidth=0;
    for (int i=0; i<graphHeight; i++){
      String temp[] = split(pixRowArray[i], "#"); 
      if ((temp.length)> maxWidth){maxWidth = temp.length;}
    }
    maxWidth = maxWidth*9;

    pgGraph = createGraphics(maxWidth, graphHeight);
    pgGraph.beginDraw();
    pgGraph.background(0,0);
    
    for (int i=0; i<graphHeight; i++){
      
      int pixColCount=0;
      String tempFace[] = split(pixRowArray[i], "#");
 
      if (tempFace.length>2){
        
        for (int j=0; j<tempFace.length; j++){   
          String tempVert [] = split(tempFace[j], ",");
          for (int k=0; k<9; k++){
            float tempCol = map(float(tempVert[k]), boundMax[0], boundMax[1], -colRangeMax, 1);;
            color pixCol = int(tempCol);
            pgGraph.set(pixColCount, graphHeight-i, pixCol);
            pixColCount++;
            
          }         
        }  
        
      }
      
    }
    
    pgGraph.updatePixels();
    pgGraph.endDraw();
    pgGraph.save("data/loaded/pixGraph" + txtSTL + "-PixelGraph.png");
    
  }
  
   void sortGeom(){
    
 //////CREATE A GRAPH OF FACES////////////////////////////////////////////////////////    
    for (int r=0; r<graphHeight; r++) {
      pixRowArray[r]="#";
    }
    
    ///CREATE TEXT FILE IN GRAPH STRUCTURE
    for (int i=0; i<fList.size(); i++) { 
        String temp[] = split(fList.get(i), ",");
        float tempZVal = (float(temp[2]) + float(temp[5]) + float(temp[8]))/3; 
        int row = int(map(tempZVal, boundZ[0], boundZ[1], 0, graphHeight));
        pixRowArray[row] = pixRowArray[row] + fList.get(i) +"#";
    }
    
    ///CLEAN UP TEXT FILE
    for (int i=0; i<graphHeight; i++){
      if (pixRowArray[i].length()>1){
        pixRowArray[i] = pixRowArray[i].substring(0, pixRowArray[i].length()-1);
        pixRowArray[i] = pixRowArray[i].substring(1);
      } else {
        pixRowArray[i] = "";
      } 
    }

//    saveStrings("data/loaded/pixGraph" + index + "-Face.txt", pixRowArray);
 
  }

  void meshDraw(){  
    
    float[] colPosA = new float[3];
    float[] colPosB = new float[3];
    float[] colPosC = new float[3];

    int faceCount=0;
    int matchIndex;   

    strokeWeight(1/SCALE);
    stroke(0, 20); 
    fill(255,0,0);

    for (int y=0; y<pixRowArray.length; y++) {
      
      fill(255);  
          
      String[] tempFace = splitTokens(pixRowArray[y], "#");   
                 
      for (int x=0; x<tempFace.length; x++) { 
        
        String[] tempPt = splitTokens(tempFace[x], ",");
     
        colPosA[0]= map(float(tempPt[0]), boundMax[0], boundMax[1], -displayBox/2, displayBox/2);
        colPosA[1]= map(float(tempPt[1]), boundMax[0], boundMax[1], -displayBox/2, displayBox/2);
        colPosA[2]= map(float(tempPt[2]), boundMax[0], boundMax[1], -displayBox/2, displayBox/2);
    
        colPosB[0]= map(float(tempPt[3]), boundMax[0], boundMax[1], -displayBox/2, displayBox/2);
        colPosB[1]= map(float(tempPt[4]), boundMax[0], boundMax[1], -displayBox/2, displayBox/2);
        colPosB[2]= map(float(tempPt[5]), boundMax[0], boundMax[1], -displayBox/2, displayBox/2);
    
        colPosC[0]= map(float(tempPt[6]), boundMax[0], boundMax[1], -displayBox/2, displayBox/2);
        colPosC[1]= map(float(tempPt[7]), boundMax[0], boundMax[1], -displayBox/2, displayBox/2);
        colPosC[2]= map(float(tempPt[8]), boundMax[0], boundMax[1], -displayBox/2, displayBox/2);
        
          beginShape();
            vertex(colPosA[0], colPosA[1], colPosA[2]);
            vertex(colPosB[0], colPosB[1], colPosB[2]);
            vertex(colPosC[0], colPosC[1], colPosC[2]);
          endShape(CLOSE);

      } 
    }
  }
  
}


///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
void initializeMeshes(){
  
   for (int i=0; i<loadMeshes; i++){
     mesh = new meshObj(i);
     meshes.add(mesh); 
   }
 
}
